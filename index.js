/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import DetailsOwnerScreen from './screens/DetailsScreens/DetailsOwnerScreen';
import DetailsMemberScreen from './screens/DetailsScreens/DetailsMemberScreen';
import NotiListScreen from './screens/NotiScreens/NotiListScreen';
import RequestOwner from './screens/RequestScreens/RequestOwnerScreen';
import Test from './screens/Test';
import {name as appName} from './app.json';
import RequestMember from './screens/RequestScreens/RequestMemberScreen';

AppRegistry.registerComponent(appName, () => () => <DetailsMemberScreen />);
