export type ResponseApiDataType = {
  result: [
    {
      name: {
        title: string;
        first: string;
        last: string;
      };
      email: string;
      phone?: string;
      picture: {
        large?: string;
        medium?: string;
        thumbnail?: string;
      };
    },
  ];
};

export type UserInfoItem = {
  name: {
    title: string;
    first: string;
    last: string;
  };
  email: string;
  phone?: string;
  picture: {
    large?: string;
    medium?: string;
    thumbnail?: string;
  };
};
