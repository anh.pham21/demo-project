export type PendingDataType = {
  price: string;
  date: string;
  name: string;
  status: string;
  doneMember: number;
  totalMember: number;
  isPending?: boolean;
};
