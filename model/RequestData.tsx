export type RequestDataType = {
  userName: string;
  avatar: any;
  requestName: string;
  value: string;
  isRequest?: boolean;
  isOwner?: boolean;
};
