import React from 'react';

import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';

const RequestHeader = () => {
  return (
    <View style={styles.statusBar}>
      <Image
        source={{
          uri: 'https://icons.veryicon.com/png/o/miscellaneous/arrows/go-back-2.png',
        }}
        style={styles.backIcon}
      />
      <Text style={styles.title}>Yêu cầu chờ duyệt</Text>
      <TouchableOpacity>
        <Text style={styles.create}>Tạo</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  statusBar: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 25,
    paddingHorizontal: 12,
  },
  backIcon: {
    width: 10,
    height: 16,
    tintColor: '#fff',
  },
  title: {
    color: '#fff',
    fontSize: 18,
    fontWeight: '700',
  },
  create: {
    fontSize: 15,
    lineHeight: 22,
    fontWeight: '700',
    color: '#fff',
  },
});

export default RequestHeader;
