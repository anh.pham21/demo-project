import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {RequestDataType} from '../../../model/RequestData';

interface RequestBtnProps {
  item: RequestDataType;
}

const RequestBtn = (props: RequestBtnProps) => {
  const {item} = props || {};
  const {isOwner, isRequest} = item || {};
  return (
    <View style={styles.btnContainer}>
      {isOwner && (
        <>
          <TouchableOpacity style={styles.acceptBtn}>
            <Text style={styles.acceptBtnText}>Chấp nhận</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.decideBtn}>
            <Text style={styles.decideBtnText}>Từ chối</Text>
          </TouchableOpacity>
        </>
      )}
      {isRequest && (
        <TouchableOpacity style={styles.cancelBtn}>
          <Text style={styles.cancelBtnText}>Huỷ</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  btnContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },

  decideBtn: {
    paddingHorizontal: 14,
    gap: 4,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#ccc',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 8,
  },

  decideBtnText: {
    fontSize: 13,
    fontWeight: '600',
    lineHeight: 22,
  },

  acceptBtn: {
    paddingHorizontal: 14,
    borderRadius: 6,
    backgroundColor: '#EB2F96',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 8,
  },

  acceptBtnText: {
    fontSize: 13,
    fontWeight: '600',
    lineHeight: 22,
    color: '#fff',
  },

  cancelBtn: {
    paddingHorizontal: 14,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#ccc',
    width: 86,
    justifyContent: 'center',
    alignItems: 'center',
  },

  cancelBtnText: {
    fontSize: 13,
    fontWeight: '600',
    lineHeight: 22,
  },
});

export default RequestBtn;
