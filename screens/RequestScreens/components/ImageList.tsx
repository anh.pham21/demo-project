import React from 'react';
import {Image, FlatList, StyleSheet} from 'react-native';

const imgList = [
  {
    img: require('../../../img/tw.jpeg'),
  },
  {
    img: require('../../../img/tw.jpeg'),
  },
  {
    img: require('../../../img/tw.jpeg'),
  },
];
const ImageList = () => {
  return (
    <FlatList
      data={imgList}
      horizontal={true}
      renderItem={({item}) => {
        return <Image source={item.img} style={styles.image} />;
      }}
    />
  );
};

const styles = StyleSheet.create({
  image: {
    width: 54,
    height: 54,
    borderRadius: 12,
    resizeMode: 'stretch',
    marginRight: 12,
    marginVertical: 12,
  },
});

export default ImageList;
