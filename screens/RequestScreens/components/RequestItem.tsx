import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';

import ImageList from './ImageList';
import RequestValue from './RequestValue';
import RequestBtn from './RequestBtn';
import {RequestDataType} from '../../../model/RequestData';

interface RequestItemProps {
  item: RequestDataType;
}

const RequestItem = (props: RequestItemProps) => {
  const {item} = props || {};
  const {avatar, userName, requestName} = item || {};
  return (
    <View style={styles.memberItem}>
      <Image source={avatar} style={styles.memberAvatar} />
      <View style={styles.memberInfor}>
        <View>
          <Text style={styles.memberName}>{userName}</Text>
          <Text style={styles.memberRequestName}>{requestName}</Text>
          <ImageList />
          <RequestBtn item={item} />
        </View>
      </View>
      <RequestValue item={item} />
    </View>
  );
};

const styles = StyleSheet.create({
  memberItem: {
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: '#F2F2F6',
    marginBottom: 20,
  },

  memberAvatar: {
    width: 40,
    height: 40,
    resizeMode: 'stretch',
    borderRadius: 100,
  },

  memberInfor: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginLeft: 8,
  },

  memberName: {
    fontSize: 12,
    lineHeight: 12,
    fontWeight: '600',
  },

  memberRequestName: {
    fontSize: 12,
    lineHeight: 16,
    fontWeight: '400',
    color: '#727272',
  },
});

export default RequestItem;
