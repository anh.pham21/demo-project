import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {RequestDataType} from '../../../model/RequestData';

interface RequestValueProps {
  item: RequestDataType;
}

const RequestValue = (props: RequestValueProps) => {
  const {item} = props || {};
  const {value} = item || {};
  return (
    <View style={styles.requestValue}>
      <Text style={styles.memberValue}>Yêu cầu rút</Text>
      <Text style={styles.memberValue}>{value}đ</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  requestValue: {
    alignItems: 'flex-end',
  },

  memberValue: {
    fontSize: 12,
    lineHeight: 16,
    fontWeight: '700',
  },
});

export default RequestValue;
