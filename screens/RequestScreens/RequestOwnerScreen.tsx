import React from 'react';
import RequestHeader from './components/RequestHeader';
import RequestItem from './components/RequestItem';
import {RequestDataType} from '../../model/RequestData';

import {
  View,
  SafeAreaView,
  StyleSheet,
  FlatList,
  StatusBar,
} from 'react-native';

const requestData = [
  {
    userName: 'Trần Thanh Hùng',
    avatar: require('../../img/ava.png'),
    requestName: 'tiền đặt xe khách',
    value: '500.000',
    isOwner: true,
  },
  {
    userName: 'Trần Thanh Hùng',
    avatar: require('../../img/ava.png'),
    requestName: 'tiền đặt xe khách',
    value: '500.000',
    isOwner: true,
  },
];

const RequestOwner = () => {
  const requestItem = ({item}: {item: RequestDataType}) => {
    return <RequestItem item={item} />;
  };

  return (
    <View style={styles.fullscreen}>
      <SafeAreaView>
        <StatusBar barStyle={'light-content'} />
        <RequestHeader />
        <View>
          <FlatList data={requestData} renderItem={requestItem} />
        </View>
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  fullscreen: {
    flex: 1,
    backgroundColor: '#F2F2F6',
    // backgroundColor: '#000',
    paddingHorizontal: 12,
  },
});

export default RequestOwner;
