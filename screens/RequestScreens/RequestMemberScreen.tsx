import React from 'react';
import RequestHeader from './components/RequestHeader';
import RequestItem from './components/RequestItem';
import {RequestDataType} from '../../model/RequestData';

import {
  View,
  SafeAreaView,
  StyleSheet,
  FlatList,
  StatusBar,
} from 'react-native';

// Fake Data
const requestData = [
  {
    userName: 'Trần Thanh Hùng',
    avatar: require('../../img/ava.png'),
    requestName: 'tiền đặt xe khách',
    value: '500.000',
    isRequest: true,
  },
  {
    userName: 'Trần Thanh Hùng',
    avatar: require('../../img/ava.png'),
    requestName: 'tiền đặt xe khách',
    value: '500.000',
    isRequest: true,
  },
];

const Request_Member = () => {
  // Request Item Component
  const requestItem = ({item}: {item: RequestDataType}) => {
    return <RequestItem item={item} />;
  };

  // Main Component
  return (
    <View style={styles.fullscreen}>
      <SafeAreaView>
        <StatusBar barStyle={'light-content'} />
        <RequestHeader />
        <View>
          <FlatList data={requestData} renderItem={requestItem} />
        </View>
      </SafeAreaView>
    </View>
  );
};

// Style Section

const styles = StyleSheet.create({
  fullscreen: {
    flex: 1,
    backgroundColor: '#F2F2F6',
    // backgroundColor: '#000',
    paddingHorizontal: 12,
  },
});

export default Request_Member;
