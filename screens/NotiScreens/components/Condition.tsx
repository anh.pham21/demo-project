import React from 'react';

import {View, Text, StyleSheet} from 'react-native';
import {PendingDataType} from '../../../model/PendingData';

interface ConditionProps {
  item: PendingDataType;
}

const Condition = (props: ConditionProps) => {
  const {item} = props || {};
  const {name, status} = item || {};
  return (
    <View style={styles.conditionContainer}>
      <View style={styles.conditionBar}>
        <Text style={styles.conditionBarFont}>{name}</Text>
      </View>
      <Text style={styles.conditionFont}>{status}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  conditionContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  conditionBar: {
    paddingHorizontal: 4,
    paddingVertical: 8,
    justifyContent: 'center',
    borderRadius: 8,
    backgroundColor: '#F9F9F9',
    flex: 1,
    marginRight: 8,
  },
  conditionBarFont: {
    fontSize: 10,
    fontWeight: '400',
    lineHeight: 10,
    color: '#727272',
  },
  conditionFont: {
    fontSize: 12,
    fontWeight: '400',
    lineHeight: 16,
  },
});

export default Condition;
