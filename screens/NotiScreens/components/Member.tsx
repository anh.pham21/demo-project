import React from 'react';

import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {PendingDataType} from '../../../model/PendingData';

interface MemberProps {
  item: PendingDataType;
}

const Member = (props: MemberProps) => {
  const {item} = props || {};
  const {doneMember, totalMember} = item || {};
  return (
    <View style={styles.memberContainer}>
      <View style={styles.memberInfor}>
        <Text style={styles.memberFont}>Đã góp:</Text>
        <Text style={(styles.memberFont, styles.memberBold)}>{doneMember}</Text>
        <Text style={styles.memberFont}>/ {totalMember} người</Text>
      </View>
      {item?.isPending && (
        <TouchableOpacity style={styles.submitBtn}>
          <Text style={styles.submitText}>Góp quỹ</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  memberContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  memberContainerDone: {
    flexDirection: 'row',
  },
  memberInfor: {
    flexDirection: 'row',
  },
  memberFont: {
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 16,
    color: '#727272',
  },
  memberBold: {
    fontWeight: '700',
    color: '#000',
    marginLeft: 4,
    marginRight: 4,
  },
  submitBtn: {
    backgroundColor: '#EB2F96',
    paddingVertical: 8,
    paddingHorizontal: 16,
    justifyContent: 'center',
    alignItems: 'center',
    gap: 8,
    borderRadius: 8,
  },
  submitText: {
    color: '#fff',
    fontSize: 12,
    fontWeight: '700',
    lineHeight: 16,
  },
});

export default Member;
