import React from 'react';

import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {PendingDataType} from '../../../model/PendingData';

interface PriceProps {
  item: PendingDataType;
}

const Price = (props: PriceProps) => {
  const {item} = props || {};
  const {price, date} = item || {};
  return (
    <View style={styles.priceContainer}>
      <Text style={styles.priceFont}>{price}</Text>
      <View style={styles.row}>
        <Text style={styles.dateFont}>{date}</Text>
        <TouchableOpacity>
          <Image
            source={{
              uri: 'https://img.mservice.io/momo_app_v2/new_version/img/appx_icon/16_navigation_more_horiz.png',
            }}
            style={styles.moreBtn}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
  },

  moreBtn: {
    width: 16,
    height: 16,
    tintColor: '#727272',
  },
  priceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: -10,
  },
  priceFont: {
    fontWeight: '600',
    fontSize: 14,
    lineHeight: 20,
  },
  dateFont: {
    fontWeight: '400',
    fontSize: 10,
    lineHeight: 14,
    marginRight: 8,
  },
});

export default Price;
