import React from 'react';

import {View, StyleSheet} from 'react-native';

import Condition from './Condition';
import Member from './Member';
import Price from './Price';
import {PendingDataType} from '../../../model/PendingData';

interface ListItemProps {
  item: PendingDataType;
}

const ListItem = (props: ListItemProps) => {
  const {item} = props || {};
  return (
    <View style={styles.processContainer}>
      <Price item={item} />
      <Condition item={item} />
      <Member item={item} />
    </View>
  );
};

const styles = StyleSheet.create({
  processContainer: {
    gap: 12,
    padding: 12,
    borderRadius: 8,
    backgroundColor: 'white',
    flexDirection: 'column',
    marginTop: 12,
  },
});

export default ListItem;
