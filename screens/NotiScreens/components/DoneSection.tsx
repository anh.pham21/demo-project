import React from 'react';
import ListItem from './ListItem';
import {PendingDataType} from '../../../model/PendingData';

import {View, Text, StyleSheet, FlatList} from 'react-native';

const doneData = [
  {
    price: '100.000đ',
    date: '01/04/2024',
    name: 'Góp quỹ tháng 4',
    status: 'Đã xong',
    doneMember: 6,
    totalMember: 11,
    isPending: false,
  },
  {
    price: '100.000đ',
    date: '01/04/2024',
    name: 'Góp quỹ tháng 4',
    status: 'Đã huỷ',
    doneMember: 7,
    totalMember: 11,
    isPending: false,
  },
  {
    price: '100.000đ',
    date: '01/04/2024',
    name: 'Góp quỹ tháng 4',
    status: 'Hết hạn',
    doneMember: 8,
    totalMember: 11,
    isPending: false,
  },
];

const DoneSection = () => {
  // Done Component Section
  const doneItem = ({item}: {item: PendingDataType}) => {
    return <ListItem item={item} />;
  };

  return (
    <View>
      <Text style={styles.doneTitle}>Đã xong</Text>
      <FlatList data={doneData} renderItem={doneItem} />
    </View>
  );
};

const styles = StyleSheet.create({
  doneTitle: {
    fontSize: 14,
    lineHeight: 20,
    fontWeight: '600',
    marginTop: 12,
  },
});

export default DoneSection;
