import React from 'react';
import ListItem from './ListItem';
import {PendingDataType} from '../../../model/PendingData';

import {View, Text, StyleSheet, FlatList} from 'react-native';

const pendingData = [
  {
    price: '100.000đ',
    date: '01/04/2024',
    name: 'Góp quỹ tháng 4',
    status: 'Đang chờ',
    doneMember: 6,
    totalMember: 11,
    isPending: true,
  },
];

const PendingSection = () => {
  // Pending Component Section
  const pendingItem = ({item}: {item: PendingDataType}) => {
    return <ListItem item={item} />;
  };
  return (
    <View>
      <Text style={styles.pendingTitle}>Đang thu</Text>
      <FlatList data={pendingData} renderItem={pendingItem} />
    </View>
  );
};

const styles = StyleSheet.create({
  pendingTitle: {
    fontSize: 14,
    lineHeight: 20,
    fontWeight: '600',
    marginTop: 15,
  },
});

export default PendingSection;
