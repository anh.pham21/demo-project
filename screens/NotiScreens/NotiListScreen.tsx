import React from 'react';
import NotiHeader from './components/NotiHeader';
import PendingSection from './components/PendingSection';
import DoneSection from './components/DoneSection';

import {View, SafeAreaView, StyleSheet, StatusBar} from 'react-native';

const NotiListScreen = () => {
  // Main Component Section
  return (
    <View style={styles.fullscreen}>
      <SafeAreaView>
        <StatusBar barStyle={'light-content'} />
        {/* Navigation Section */}
        <NotiHeader />
        {/* Content Section */}
        <View>
          {/* Pending Section */}
          <PendingSection />
          {/* Done Section */}
          <DoneSection />
        </View>
      </SafeAreaView>
    </View>
  );
};

// Style Section
const styles = StyleSheet.create({
  fullscreen: {
    flex: 1,
    backgroundColor: '#F2F2F6',
    // backgroundColor: '#000',
    paddingHorizontal: 12,
  },
});

export default NotiListScreen;
