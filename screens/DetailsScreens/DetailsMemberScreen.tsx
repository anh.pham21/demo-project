import axios from 'axios';
import React, {useEffect, useState} from 'react';

import Header from './components/DetailsHeader';
import InformationSection from './components/InformationSection';
import MemberItem from './components/MemberItem';
import DetailsMemSubmitBtn from './components/DetailsMemSubmitBtn';

import {UserInfoItem} from '../../model/ResponseApiData';

import {
  View,
  SafeAreaView,
  Text,
  StyleSheet,
  FlatList,
  StatusBar,
  ActivityIndicator,
} from 'react-native';

const DetailsMemberScreen = () => {
  // Create a state memberData to store API Information
  const [memberData, setMemberData] = useState<UserInfoItem[]>([]);

  const [currentPage, setCurrentPage] = useState<number>(1);

  const [isLoading, setIsLoading] = useState<boolean>(false);

  // Call API and push Information in state
  useEffect(() => {
    setIsLoading(true);
    axios
      .get(`https://randomuser.me/api/?page=${currentPage}&results=10&seed=abc`)
      .then(res => {
        setMemberData(preValue => [...preValue, ...res.data.results]);
        setIsLoading(false);
      })
      .catch(() => {
        setMemberData([]);
      });
  }, [currentPage]);

  // Member Item Component
  const memberItem = ({item}: {item: UserInfoItem}) => {
    return <MemberItem item={item} />;
  };

  // Load Activity Component
  const renderLoader = () => {
    return isLoading ? (
      <View style={styles.loaderStyle}>
        <ActivityIndicator size={'large'} color={'#000'} />
      </View>
    ) : null;
  };

  // Load Item Component
  const loadMoreItem = () => {
    setCurrentPage(currentPage + 1);
  };

  return (
    <View style={styles.fullScreen}>
      <SafeAreaView>
        <StatusBar barStyle={'light-content'} />
        {/* Navigation Section */}
        <Header />
        {/* Body Section */}
        <View style={styles.body}>
          {/* Information Section */}
          <InformationSection
            message={'Tiền trà sữa farewell đồng nghiệp đáng iuuu!'}
          />
          {/* Member List Section */}
          <View>
            <Text style={styles.memberTitle}>
              Danh sách góp quỹ ({memberData.length})
            </Text>
            <View style={styles.memberContainer}>
              <FlatList
                data={memberData}
                renderItem={memberItem}
                keyExtractor={item => item.email}
                ListFooterComponent={renderLoader}
                onEndReached={loadMoreItem}
                onEndReachedThreshold={0}
              />
            </View>
          </View>
        </View>
      </SafeAreaView>

      {/* Submit Button */}
      <DetailsMemSubmitBtn />
    </View>
  );
};

// Style Section
const styles = StyleSheet.create({
  fullScreen: {
    flex: 1,
    backgroundColor: '#F2F2F6',
    // backgroundColor: '#000',
    position: 'relative',
  },

  body: {
    paddingHorizontal: 12,
  },

  statusBar: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 25,
    paddingHorizontal: 12,
  },
  backIcon: {
    width: 10,
    height: 16,
    tintColor: '#fff',
  },
  title: {
    color: '#fff',
    fontSize: 18,
    fontWeight: '700',
  },
  moreIcon: {
    width: 16,
    height: 4,
    tintColor: '#fff',
  },

  memberTitle: {
    fontSize: 16,
    fontWeight: '700',
    lineHeight: 22,
    color: '#000',
    marginTop: 20,
  },

  memberContainer: {
    backgroundColor: '#fff',
    padding: 12,
    borderRadius: 12,
    marginTop: 12,
  },

  loaderStyle: {
    alignItems: 'center',
    paddingVertical: 8,
  },
});

export default DetailsMemberScreen;
