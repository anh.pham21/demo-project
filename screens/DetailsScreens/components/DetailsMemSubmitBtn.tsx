import React from 'react';
import {TouchableOpacity, Text, View, StyleSheet} from 'react-native';

const DetailsMemSubmitBtn = () => {
  return (
    <View style={styles.submitContainer}>
      <TouchableOpacity style={styles.submitBtn}>
        <Text style={styles.submitText}>Góp quỹ</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  submitContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingHorizontal: 12,
    width: '100%',
    paddingBottom: 32,
    backgroundColor: '#fff',
    position: 'absolute',
    bottom: 0,
  },

  submitBtn: {
    paddingVertical: 13,
    gap: 8,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#EB2F96',
    width: '100%',
    marginTop: 8,
  },

  submitText: {
    color: '#fff',
    fontSize: 15,
    lineHeight: 22,
    fontWeight: '700',
  },
});

export default DetailsMemSubmitBtn;
