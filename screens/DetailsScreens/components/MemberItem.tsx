import React from 'react';

import {View, Text, Image, StyleSheet} from 'react-native';

import {UserInfoItem} from '../../../model/ResponseApiData';
interface MemberItemProps {
  item: UserInfoItem;
}

const MemberItem = (props: MemberItemProps) => {
  const {item} = props || {};
  const {name, picture, email} = item || {};
  const {title, first, last} = name || {};
  const {large} = picture || {};
  return (
    <View style={styles.memberItem}>
      <Image source={{uri: large}} style={styles.memberAvatar} />
      <View style={styles.memberInfor}>
        <View>
          <Text style={styles.memberName}>{`${title} ${first} ${last}`}</Text>
          <Text style={styles.memberPhone}>{email}</Text>
        </View>
        {/* <Text style={[styles.memberStatus]}>{item.gender}</Text> */}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  memberItem: {
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: '#fff',
    marginBottom: 8,
  },

  memberAvatar: {
    width: 40,
    height: 40,
    resizeMode: 'stretch',
    borderRadius: 100,
  },

  memberInfor: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginLeft: 8,
  },

  memberName: {
    fontSize: 12,
    lineHeight: 12,
    fontWeight: '600',
  },

  memberPhone: {
    fontSize: 12,
    lineHeight: 16,
    fontWeight: '400',
    color: '#727272',
  },

  memberStatus: {
    fontSize: 12,
    lineHeight: 16,
    fontWeight: '400',
  },
});

export default MemberItem;
