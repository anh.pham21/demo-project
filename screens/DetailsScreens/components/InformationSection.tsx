import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import DetailsImgList from './DetailsImgList';
import DetailsPrice from './DetailsPrice';

interface InformationSectionProps {
  message: string;
}

const InformationSection = (props: InformationSectionProps) => {
  const {message} = props || {};

  return (
    <View style={styles.infor}>
      <DetailsPrice />
      <View style={styles.inforTitle}>
        <Text>{message}</Text>
      </View>
      <View style={styles.inforImg}>
        <DetailsImgList />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  infor: {
    backgroundColor: '#fff',
    borderRadius: 12,
    gap: 12,
  },
  inforTitle: {
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 20,
    color: '#303233',
    marginHorizontal: 12,
  },
  inforImg: {
    marginHorizontal: 12,
  },
});

export default InformationSection;
