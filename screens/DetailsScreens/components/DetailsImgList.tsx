import React from 'react';
import {Image, FlatList, StyleSheet} from 'react-native';

const imgList = [
  {
    img: require('../../../img/tw.jpeg'),
  },
  {
    img: require('../../../img/tw.jpeg'),
  },
  {
    img: require('../../../img/tw.jpeg'),
  },
];

const DetailsImgList = () => {
  return (
    <FlatList
      data={imgList}
      horizontal={true}
      renderItem={({item}) => {
        return <Image source={item.img} style={styles.image} />;
      }}
    />
  );
};

const styles = StyleSheet.create({
  image: {
    width: 72,
    height: 72,
    borderRadius: 12,
    gap: 12,
    resizeMode: 'stretch',
    marginBottom: 12,
    marginRight: 12,
  },
});

export default DetailsImgList;
