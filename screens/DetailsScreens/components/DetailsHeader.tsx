import React from 'react';
import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';

const DetailsHeader = () => {
  return (
    <View style={styles.statusBar}>
      <TouchableOpacity>
        <Image
          source={{
            uri: 'https://icons.veryicon.com/png/o/miscellaneous/arrows/go-back-2.png',
          }}
          style={styles.backIcon}
        />
      </TouchableOpacity>
      <Text style={styles.title}>Chi tiết góp quỹ</Text>
      <TouchableOpacity>
        <Image
          source={require('../../../img/16_navigation_more_horiz.webp')}
          style={styles.moreIcon}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  statusBar: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 25,
    paddingHorizontal: 12,
  },
  backIcon: {
    width: 10,
    height: 16,
    tintColor: '#fff',
  },
  title: {
    color: '#fff',
    fontSize: 18,
    fontWeight: '700',
  },
  moreIcon: {
    width: 16,
    height: 4,
    tintColor: '#fff',
  },
});

export default DetailsHeader;
