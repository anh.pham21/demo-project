import React from 'react';
import {Text, View, StyleSheet} from 'react-native';

const DetailsPrice = () => {
  return (
    <View style={styles.inforPrice}>
      <Text style={styles.priceDesc}>Tổng tiền cần chia</Text>
      <Text style={styles.price}>100.000đ</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  inforPrice: {
    padding: 12,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#727272',
  },
  priceDesc: {
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 20,
    color: '#727272',
  },
  price: {
    fontSize: 18,
    fontWeight: '700',
    lineHeight: 28,
    color: '#000',
  },
});

export default DetailsPrice;
