import React, {useEffect, useState} from 'react';
import axios from 'axios';

import Header from './components/DetailsHeader';
import InformationSection from './components/InformationSection';
import MemberItem from './components/MemberItem';
import DetailsOwnerSubmitBtn from './components/DetailsOwnerSubmitBtn';

import {UserInfoItem} from '../../model/ResponseApiData';

import {
  View,
  SafeAreaView,
  Text,
  StyleSheet,
  FlatList,
  StatusBar,
  ActivityIndicator,
} from 'react-native';

const DetailsOwnerScreen = () => {
  // Create a state memberData to store API Information
  const [memberData, setMemberData] = useState<UserInfoItem[]>([]);

  const [currentPage, setCurrentPage] = useState<number>(1);

  const [isLoading, setIsLoading] = useState<boolean>(false);

  // Push Information in state
  useEffect(() => {
    setIsLoading(true);
    axios
      .get(`https://randomuser.me/api/?page=${currentPage}&results=10&seed=abc`)
      .then(res => {
        setMemberData(preValue => [...preValue, ...res.data.results]);
        setIsLoading(false);
      })
      .catch(() => {
        setMemberData([]);
      });
  }, [currentPage]);

  const memberItem = ({item}: {item: UserInfoItem}) => {
    // Member Item Component
    return <MemberItem item={item} />;
  };

  // Load Activity Component
  const renderLoader = () => {
    return isLoading ? (
      <View>
        <ActivityIndicator size={'large'} color={'#000'} />
      </View>
    ) : null;
  };

  const loadMoreItem = () => {
    setCurrentPage(currentPage + 1);
  };
  return (
    <View style={styles.fullScreen}>
      <SafeAreaView>
        <StatusBar barStyle={'light-content'} />
        {/* Navigation */}
        <Header />
        <View style={styles.body}>
          <InformationSection message={'Góp quỹ tháng 4'} />
          <View>
            <Text style={styles.memberTitle}>
              Danh sách góp quỹ ({memberData.length})
            </Text>
            <View style={styles.memberContainer}>
              <FlatList
                data={memberData}
                renderItem={memberItem}
                keyExtractor={item => item.email}
                ListFooterComponent={renderLoader}
                onEndReached={loadMoreItem}
                onEndReachedThreshold={0}
              />
            </View>
          </View>
        </View>
      </SafeAreaView>
      <DetailsOwnerSubmitBtn />
    </View>
  );
};

// Style Section
const styles = StyleSheet.create({
  fullScreen: {
    flex: 1,
    backgroundColor: '#F2F2F6',
    // backgroundColor: '#000',
    position: 'relative',
  },

  body: {
    paddingHorizontal: 12,
  },

  memberTitle: {
    fontSize: 16,
    fontWeight: '700',
    lineHeight: 22,
    color: '#000',
    marginTop: 20,
  },

  memberContainer: {
    backgroundColor: '#fff',
    padding: 12,
    borderRadius: 12,
    marginTop: 12,
  },
});

export default DetailsOwnerScreen;
