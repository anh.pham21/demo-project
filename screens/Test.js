import {Component, useState} from 'react';
import React from 'react';

import {
  Text,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
  FlatList,
  SafeAreaView,
  StyleSheet,
  TextInput,
} from 'react-native';

const Test = () => {
  const baseData = [
    {
      id: 0,
      name: '',
    },
  ];

  const [data, setData] = useState(baseData);
  const [value, setValue] = useState(0);
  const [valueArr, setArr] = useState([]);

  const [inputData, setInputData] = useState([]);
  const [userInput, setUserInput] = useState('');

  const valueItem = ({item, index}) => {
    return (
      <View style={styles.resultItem}>
        <Text>{item}</Text>
        <TouchableOpacity onPress={delRequestList}>
          <Text>Delete</Text>
        </TouchableOpacity>
      </View>
    );
  };

  const pushArr = () => {
    setValue(value + 1);
    setArr([...valueArr, value]);
  };

  const pushRequestList = () => {
    setInputData([...inputData, userInput]);
  };

  const pushData = () => {
    setData(data.id + 1);
  };

  console.log(inputData);

  const getUserInput = text => {
    setUserInput(text);
  };

  const resetUserInput = text => {
    setUserInput('');
  };

  const delRequestList = ({index}) => {
    const newData = [...inputData];
    newData.splice(index, 1);
    setInputData(newData);
  };

  return (
    <View style={styles.fullScreen}>
      <SafeAreaView style={styles.screen}>
        <StatusBar barStyle={'light-content'} />
        <View style={styles.statusBar}>
          <TouchableOpacity style={styles.barBtn}>
            <Image
              source={require('../img/back-icon.png')}
              style={styles.backIcon}
            />
          </TouchableOpacity>
          <Text style={styles.title}>Test Screen</Text>
          <TouchableOpacity style={styles.barBtn}>
            <Image
              source={require('../img/16_navigation_more_horiz.webp')}
              style={styles.moreIcon}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.body}>
          <View style={styles.form}>
            <Text style={styles.formTitle}>Create Request</Text>
            <View style={styles.formContainer}>
              <TextInput
                style={styles.formInput}
                placeholder="Enter your request"
                onChangeText={getUserInput}
              />
              <TouchableOpacity>
                <Text onPress={resetUserInput}>Reset</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.submitBtn}
                onPress={pushRequestList}>
                <Text style={styles.submitBtnText}>Send</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.result}>
            <Text style={styles.formTitle}>Result</Text>
            <View style={styles.resultContainer}>
              <FlatList
                data={inputData}
                renderItem={valueItem}
                keyExtractor={(item, index) => `${item}_${index}`}
              />
            </View>
          </View>
        </View>
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  fullScreen: {
    flex: 1,
    // backgroundColor: '#F2F2F6',
    backgroundColor: '#000',
  },

  body: {
    paddingHorizontal: 12,
  },

  statusBar: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 25,
    paddingHorizontal: 12,
  },

  backIcon: {
    width: 10,
    height: 16,
    tintColor: '#fff',
  },
  title: {
    color: '#fff',
    fontSize: 18,
    fontStyle: 'SF Pro Text',
    fontWeight: '700',
  },
  moreIcon: {
    width: 16,
    height: 4,
    tintColor: '#fff',
  },
  formTitle: {
    color: '#fff',
    fontSize: 20,
    fontStyle: 'SF Pro Text',
    fontWeight: '700',
  },
  formContainer: {
    backgroundColor: '#fff',
    borderRadius: 8,
    padding: 12,
    marginVertical: 12,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  formInput: {
    width: '70%',
    borderWidth: 1,
    borderRadius: 8,
    padding: 4,
  },
  resultContainer: {
    backgroundColor: '#fff',
    borderRadius: 8,
    padding: 12,
    marginVertical: 12,
    alignItems: 'center',
  },
  resultItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    padding: 12,
  },
});

export default Test;
