var promise = new Promise((resolve, reject) => {
  resolve();
});

promise
  .then(() => {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(1);
      }, 1000);
    });
  })
  .then(data => {
    console.log(data);
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(2);
      }, 1000);
    });
  })
  .then(data => {
    console.log(data);
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(3);
      }, 1000);
    });
  })
  .then(data => {
    console.log(data);
  });

var sleep = ms => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve();
    }, 1000);
  });
};

sleep(1000)
  .then(() => {
    console.log(1);
    sleep(1000);
  })
  .then(() => {
    console.log(2);
    sleep(1000);
  })
  .then(() => {
    console.log(3);
    sleep(1000);
  });

var promise1 = new Promise((resolve, reject) => {
  resolve();
});

promise1
  .then(() => {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(['1', '2']);
      }, 1000);
    });
  })

  .then(data => {
    console.log(data);
  });

var promise2 = new Promise((resolve, reject) => {
  resolve();
});

promise2
  .then(() => {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(['3']);
      }, 1000);
    });
  })

  .then(data => {
    console.log(data);
  });

const user = [
  {userId: 1, name: 'DA'},
  {userId: 2, name: 'DA1'},
  {userId: 3, name: 'DA2'},
];

const comment = [
  {id: 1, uID: 1, content: 'xin'},
  {id: 2, uID: 2, content: 'chao'},
  {id: 3, uID: 3, content: 'DA'},
];

const getComment = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(comment);
    }, 1000);
  });
};

getComment().then(() => {
  const getAllUserIdsbyComment = comment.map((comment) => {
    return comment.uID;
  });
  console.log(getAllUserIdsbyComment);
  const getUserIdsbyComment = getAllUserIdsbyComment.filter(1, 2);
  console.log(getUserIdsbyComment);
});
